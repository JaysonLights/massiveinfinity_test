package com.digital.bunny.massiveinfinity_test.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.digital.bunny.massiveinfinity_test.R;
import com.digital.bunny.massiveinfinity_test.activities.MainActivity;
import com.digital.bunny.massiveinfinity_test.api.core.api_helper.OnApiResponse;
import com.digital.bunny.massiveinfinity_test.api.core.api_helper.ResponsePair;
import com.digital.bunny.massiveinfinity_test.interfaces.OnActionPasser;
import com.digital.bunny.massiveinfinity_test.utils.DataHolder;
import com.digital.bunny.massiveinfinity_test.utils.Helper;
import com.digital.bunny.massiveinfinity_test.adapter.ItemAdapter;
import com.digital.bunny.massiveinfinity_test.models.DataDetail;
import com.digital.bunny.massiveinfinity_test.utils.Locator;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import retrofit.RetrofitError;

/**
 * Created by jayson on 2/22/16.
 */
public class ParkFragment extends Fragment {

    private MainActivity activity;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private ItemAdapter itemAdapter;
    private ArrayList<DataDetail> listData;
    private SwipeRefreshLayout refreshLayout;
    private AVLoadingIndicatorView animatedLoader;
    private Locator locator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity)getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout, container, false);
        set(view);

        return view;
    }

    private void set(View view){
        initView(view);
        setData();
        getData();
    }

    private void initView(View view){
        animatedLoader = (AVLoadingIndicatorView) view.findViewById(R.id.animated_loader);
        refreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_container);
        recyclerView = (RecyclerView)view.findViewById(R.id.listview);
        recyclerView.setOverScrollMode(ListView.OVER_SCROLL_NEVER);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        refreshLayout.setOnRefreshListener(refresh);
    }

    private SwipeRefreshLayout.OnRefreshListener refresh = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if(Helper.isConnected(getContext())) {
                getData();
            }else{
                refreshLayout.setRefreshing(false);
                Helper.toasterCentered(getContext(), "No internet connection");
            }
        }
    };

    public void setData() {
        initDataList();
        animatedLoader.setVisibility(View.VISIBLE);
        itemAdapter = new ItemAdapter(getActivity(), listData, recyclerView);
        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(itemAdapter);
        recyclerView.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));
        itemAdapter.notifyDataSetChanged();
    }

    private void initDataList(){
        listData = new ArrayList<>();
    }

    private void getData(){
        Helper.toasterCentered(getContext(), "Getting your current location..\nPlease wait..");
        locator = new Locator(getContext(), actionPasser);
    }

    private OnActionPasser actionPasser = new OnActionPasser() {
        @Override
        public void onPassedDatas(DataHolder dataHolder) {
            switch (dataHolder.tag){
                case TAG_ENABLE_INTERNET:
                    getActivity().finish();
                    break;
                case TAG_ENABLE_LOCATION_SERVICE:
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    break;
                case TAG_PROGRESS_LOADER:
                    break;
                case TAG_END_LOADER:
                    Helper.toasterCentered(getContext(), "Fetching data..\nPlease wait..");
                    activity.getClientManager().getData("TEST_DATA", onDataFetch);
                    break;
            }
        }
    };

    private OnApiResponse<ResponsePair<List<DataDetail>>> onDataFetch = new OnApiResponse<ResponsePair<List<DataDetail>>>() {
        @Override
        public void onRequestSuccessful(ResponsePair<List<DataDetail>> result) {
            animatedLoader.setVisibility(View.GONE);
            listData.clear();
            listData.addAll(result.getResult());
            itemAdapter.reloadItems(listData);
            refreshLayout.setRefreshing(false);
            itemAdapter.setLoaded();
        }

        @Override
        public void onRequestFailed(String errorMessage, Object tag) {
            animatedLoader.setVisibility(View.GONE);
            activity.toasterCentered("failed");
        }

        @Override
        public void onRequestError(RetrofitError error, Object tag) {
            animatedLoader.setVisibility(View.GONE);
            activity.toasterCentered("retro failed");
        }
    };

}
