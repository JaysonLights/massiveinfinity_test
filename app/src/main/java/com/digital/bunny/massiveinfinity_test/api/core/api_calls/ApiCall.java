package com.digital.bunny.massiveinfinity_test.api.core.api_calls;

import com.digital.bunny.massiveinfinity_test.api.core.api_helper.OnApiResponse;
import com.digital.bunny.massiveinfinity_test.api.core.api_helper.ResponsePair;
import com.digital.bunny.massiveinfinity_test.models.DataDetail;

import java.util.List;

/**
 * Created by jayson on 6/25/15.
 */
public interface ApiCall {
    void getData(Object tag, final OnApiResponse<ResponsePair<List<DataDetail>>> onApiResponse);
}
