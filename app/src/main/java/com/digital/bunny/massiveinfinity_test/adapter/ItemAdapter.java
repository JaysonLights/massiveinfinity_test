package com.digital.bunny.massiveinfinity_test.adapter;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.digital.bunny.massiveinfinity_test.R;
import com.digital.bunny.massiveinfinity_test.activities.MapActivity;
import com.digital.bunny.massiveinfinity_test.models.DataDetail;
import com.digital.bunny.massiveinfinity_test.utils.ActivityTransition;
import com.digital.bunny.massiveinfinity_test.utils.BusHelper;
import com.digital.bunny.massiveinfinity_test.utils.Helper;

import java.util.List;

/**
 * Created by jayson on 1/6/16.
 */
public class ItemAdapter extends RecyclerView.Adapter {
    private Activity activity;
    private List<DataDetail> itemList;
    private RecyclerView recyclerView;
    private int lastVisibleItem, totalItemCount;
    private int visibleThreshold = 1;
    private boolean loading;

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    public ItemAdapter(Activity activity, List<DataDetail> itemList, RecyclerView recyclerView) {
        this.activity = activity;
        this.itemList = itemList;
        this.recyclerView = recyclerView;
        setLoadMoreControl();
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder) {
            final DataDetail item = itemList.get(i);
            Helper.setImage(item.getImageUrl(), ((ViewHolder) viewHolder).image);
            ((ViewHolder) viewHolder).description.setText(item.getDescription());
            ((ViewHolder) viewHolder).cellContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BusHelper.eventBusPassData(activity, new BusHelper(BusHelper.BusEvent.VIEW_MAP, item));
                    ActivityTransition.activityAnimateLauncher(activity, MapActivity.class, false);
                }
            });
        }else{
            ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_ITEM) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.cell_item, viewGroup, false);

            return new ViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.loading_item, viewGroup, false);

            return new ProgressViewHolder(itemView);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        protected LinearLayout cellContainer;
        protected TextView description;
        protected ImageView image;

        public ViewHolder(View v) {
            super(v);
            cellContainer = (LinearLayout)v.findViewById(R.id.item_container);
            description = (TextView)v.findViewById(R.id.txt);
            image = (ImageView)v.findViewById(R.id.img);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        }
    }

    private void setLoadMoreControl(){
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(
                    new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                            if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
//                                if (onLoadMoreListener != null) {
//                                    onLoadMoreListener.onLoadMore();
//                                }
                                loading = true;
                            }

                        }
                    });
        }
    }

    public void reloadItems(List<DataDetail> itemList) {
        this.itemList = itemList;
        notifyDataSetChanged();
    }

}
