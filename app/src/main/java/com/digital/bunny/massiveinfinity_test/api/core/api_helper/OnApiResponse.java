
package com.digital.bunny.massiveinfinity_test.api.core.api_helper;

import retrofit.RetrofitError;

public interface OnApiResponse<T> {
    
    public void onRequestSuccessful(T result);

    /**
     * Successfully connected to the API, but received error message due to missing parameters, invalid url, etc.
     * @param error
     * @param errorMessage 
     */
    public void onRequestFailed(String errorMessage, Object tag);
    /**
     * API is unreachable or timeout on request
     * @param error
     */
    public void onRequestError(RetrofitError error, Object tag);
}
