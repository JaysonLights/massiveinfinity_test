package com.digital.bunny.massiveinfinity_test.api.client;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;

public class CallbackManager {
    
    //TODO Create background executor for this class.
    
    private List<ResponseListener> callbackList;
    private static CallbackManager manager;
    
    public static synchronized CallbackManager getInstance(){
        if(manager == null){
            manager = new CallbackManager();
        }
        return manager;
    }
    
    public CallbackManager() {
        callbackList = new ArrayList<ResponseListener>();
    }
    
    public void addToList(ResponseListener listener){
        synchronized (callbackList) {
        callbackList.add(listener);
        }
    }

    public void cancelAll(){
        try {
            for (ResponseListener listener : callbackList) {
                synchronized (listener) {
                        listener.cancel();
                        removeFromCallbackList(listener.getTag());
                }
            }
        } catch (ConcurrentModificationException e) {

        }
    }
    
    public void cancel(Object tag) {
        if (tag != null) {
            try {
                for (ResponseListener listener : callbackList) {
                    synchronized (listener) {
                        if (listener.getTag().equals(tag)) {
                            listener.cancel();
                            removeFromCallbackList(tag);
                        }
                    }
                }
            } catch (ConcurrentModificationException e) {

            }

        }
    }

    public void removeFromCallbackList(Object tag) {
        if (tag != null) {
            synchronized (callbackList) {
                Iterator<ResponseListener> i = callbackList.iterator();
                while (i.hasNext()) {
                    ResponseListener object = i.next();
                    try{
                        if (object.getTag().equals(tag)) {
                            i.remove();
                        } 
                    }
                    catch(NullPointerException e){
                    }
                }
            }
        }
    }

}
