package com.digital.bunny.massiveinfinity_test.api.core.api_helper;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Created by jayson on 2/9/15.
 */
public class ApiResponse {

    private Object tag;
    private JsonArray jsonArryExtras;
    private JsonObject jsonExtras;
    private boolean statusSuccess;
    private String statusMessage;

    private int errorCode;
    private String errorDescription;
    private String errorReasonPhrase;
    private boolean hasData;

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    public JsonObject getRawJsonResult() {
        return jsonExtras;
    }

    public void setRawJsonResult(JsonObject jsonExtras) {
        this.jsonExtras = jsonExtras;
    }

    public JsonArray getRawJsonArrayResult() {
        return jsonArryExtras;
    }

    public void setRawJsonArrayResult(JsonArray jsonArryExtras) {
        this.jsonArryExtras = jsonArryExtras;
    }

    public boolean isStatusSuccess() {
        return statusSuccess;
    }

    public void setStatusSuccess(boolean statusSuccess) {
        this.statusSuccess = statusSuccess;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public JsonObject getJsonExtras() {
        return jsonExtras;
    }

    public void setJsonExtras(JsonObject jsonExtras) {
        this.jsonExtras = jsonExtras;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getErrorReasonPhrase() {
        return errorReasonPhrase;
    }

    public void setErrorReasonPhrase(String errorReasonPhrase) {
        this.errorReasonPhrase = errorReasonPhrase;
    }

    public boolean isHasData() {
        return hasData;
    }

    public void setHasData(boolean hasData) {
        this.hasData = hasData;
    }
}
