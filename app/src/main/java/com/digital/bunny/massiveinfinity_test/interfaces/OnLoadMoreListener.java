package com.digital.bunny.massiveinfinity_test.interfaces;

/**
 * Created by jayson on 2/22/16.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}