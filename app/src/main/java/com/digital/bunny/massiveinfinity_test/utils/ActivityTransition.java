package com.digital.bunny.massiveinfinity_test.utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.digital.bunny.massiveinfinity_test.R;

/**
 * Created by jayson on 5/21/15.
 */
public class ActivityTransition {

    public class Request{
        public static final int SETTINGS = 810;
    }

    public class Result{
        public static final int LOGOUT = 610;
    }

    public static void activityLauncher(Activity activity, Class<?> cls, boolean endActivity){
        Intent i = new Intent(activity, cls);
        activity.startActivity(i);

        if(endActivity)
            activity.finish();
    }

    public static void activityAnimatePassStringLauncher(Activity activity, Class<?> cls, String tag, String sData, boolean endActivity){
        Intent i = new Intent(activity, cls);
        i.putExtra(tag, sData);
        activity.startActivity(i);
        activity.overridePendingTransition(R.anim.open_next, R.anim.close_main);

        if(endActivity)
            activity.finish();
    }

    public static void activityAnimateLauncher(Activity activity, Class<?> cls, boolean endActivity){
        Intent i = new Intent(activity, cls);
        activity.startActivity(i);
        activity.overridePendingTransition(R.anim.open_next, R.anim.close_main);

        if(endActivity)
            activity.finish();
    }

    public static void activityForResultLauncher(Activity activity, Class<?> cls, int rCode, boolean endActivity){
        Intent i = new Intent(activity, cls);
        activity.startActivityForResult(i, rCode);
        activity.overridePendingTransition(R.anim.open_next, R.anim.close_main);

        if(endActivity)
            activity.finish();
    }

    public static void activityLauncherWithFlags(Activity activity, Class<?> cls, int flags, boolean endActivity){
        Intent i = new Intent(activity, cls);
        i.addFlags(flags);
        activity.startActivity(i);
        activity.overridePendingTransition(R.anim.open_next, R.anim.close_main);

        if(endActivity)
            activity.finish();
    }

    public static void activityActionUriLauncher(Activity activity, String action, Uri uri, int rCode, boolean endActivity){
        Intent i = new Intent(action, uri);
        activity.startActivityForResult(i, rCode);
        activity.overridePendingTransition(R.anim.open_next, R.anim.close_main);

        if(endActivity)
            activity.finish();
    }

}
