package com.digital.bunny.massiveinfinity_test.utils;

import android.content.Context;

import io.realm.Realm;

/**
 * Created by jayson on 1/5/15.
 */
public class RealmHelper {

    private boolean isStarted;
    private Realm realm;

    public Realm getRealm(){
        return realm;
    }

    private void start(Context context){
        realm = Realm.getInstance(context);
    }

    public void init(Context context){
        if(!isStarted){
            start(context);
            isStarted = true;
        }
    }

}
