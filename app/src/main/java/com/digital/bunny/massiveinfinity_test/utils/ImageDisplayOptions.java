package com.digital.bunny.massiveinfinity_test.utils;

import com.digital.bunny.massiveinfinity_test.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

/**
 * Created by jayson on 1/22/15.
 */
public class ImageDisplayOptions {

    private static ImageDisplayOptions instance;
    private DisplayImageOptions profileImageOptions;
    private CustomFadeInDisplayer customFadeInDisplayer;

    private DisplayImageOptions feedDisplayOptions;

    public static ImageDisplayOptions getInstance() {
        if (instance == null) {
            instance = new ImageDisplayOptions();
        }
        return instance;
    }

    private ImageDisplayOptions() {
        customFadeInDisplayer = new CustomFadeInDisplayer(1050, true, true, false);
        initDisplayOptions();

    }

    private void initDisplayOptions() {
        int defaultFeedImageLoader = R.drawable.default_loader_img;

        feedDisplayOptions = new DisplayImageOptions.Builder().showImageOnLoading(defaultFeedImageLoader)
                .cacheInMemory(true).displayer(customFadeInDisplayer).cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY).build();


    }

    public DisplayImageOptions getFeedDisplayOptions(){
        return feedDisplayOptions;
    }


    public DisplayImageOptions profileDisplayOptions() {
        return profileImageOptions;
    }
}
