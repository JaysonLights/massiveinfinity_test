package com.digital.bunny.massiveinfinity_test.api.deserializers;

import android.content.Context;

import com.digital.bunny.massiveinfinity_test.api.client.helper.DeserializerUtils;
import com.digital.bunny.massiveinfinity_test.api.core.api_helper.ApiResponse;
import com.digital.bunny.massiveinfinity_test.api.core.api_helper.ResponsePair;
import com.digital.bunny.massiveinfinity_test.models.DataDetail;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by jayson on 1/24/16.
 */
public class DataListDeserializer implements JsonDeserializer<ResponsePair<List<DataDetail>>> {

    private Context appContext;
    private Realm realm;

    public DataListDeserializer(Context context, Realm realm){
        this.appContext = context;
        this.realm = realm;
    }

    @Override
    public ResponsePair<List<DataDetail>> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray jsonArray = json.getAsJsonArray();
        List<DataDetail> items = new ArrayList<>();
        ApiResponse response = DeserializerUtils.extras(jsonArray);

        if(response.isHasData()) {
            JsonArray content = jsonArray;

            for(JsonElement jsonElement: content){
                try {
                    JsonObject jItem = jsonElement.getAsJsonObject();
                    DataDetail item = DeserializerUtils.updateDataItem(appContext, realm, jItem);
                    items.add(item);
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

        }
        ResponsePair<List<DataDetail>> pair = new ResponsePair<>(response, items);

        return pair;
    }
}
