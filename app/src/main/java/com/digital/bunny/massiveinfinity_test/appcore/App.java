package com.digital.bunny.massiveinfinity_test.appcore;

import android.app.Application;

import com.digital.bunny.massiveinfinity_test.api.core.ClientManager;
import com.digital.bunny.massiveinfinity_test.utils.ImageCacheConfig;
import com.digital.bunny.massiveinfinity_test.utils.RealmHelper;

import io.realm.Realm;
import io.realm.exceptions.RealmMigrationNeededException;

/**
 * Created by jayson on 1/18/15.
 */
public class App extends Application {

    private CoreEngine engine;
    private ClientManager apiBoss;
    private RealmHelper realmHelper;
    private Realm realm;
//    private GoogleAnalytics analytics;
//    private Tracker tracker;


    @Override
    public void onCreate() {
        super.onCreate();
        pseudoMigration();
        init();
    }

    private void init(){
        engine = new CoreEngine(getApplicationContext());
        apiBoss = engine.getApiBoss();
        ImageCacheConfig.init(getApplicationContext());

        realmHelper = new RealmHelper();
        realmHelper.init(getApplicationContext());
        realm = realmHelper.getRealm();
    }

    public ClientManager getClientManager(){
        return apiBoss;
    }

    public Realm getRealm(){
        return realm;
    }

    private void pseudoMigration(){
        try{
            Realm.getInstance(this);
        }
        catch(RealmMigrationNeededException e){
            e.printStackTrace();
            Realm.deleteRealmFile(this);
        }
    }

}
