package com.digital.bunny.massiveinfinity_test.utils;

import android.app.Activity;

import de.greenrobot.event.EventBus;

/**
 * Created by jayson on 11/27/14.
 */
public class BusHelper {

    public enum BusEvent{
        VIEW_MAP, ACTION_FAVE, ACTION_UNFAVE
    }

    public BusEvent event_type;
    public BusEvent busEvent;
    public Object busObject;

    public BusHelper(BusEvent evt) {
        this.event_type = evt;
    }

    public BusHelper(BusEvent busEvent, Object busObject) {
        this.busEvent = busEvent;
        this.busObject = busObject;
    }

    public static void eventBusPassEvent(Activity activity, BusEvent event) {
        EventBus.getDefault().postSticky(event);
        if (!EventBus.getDefault().isRegistered(activity))
            EventBus.getDefault().registerSticky(activity);
    }

    public static void eventBusPassData(Activity activity, BusHelper bus) {
        EventBus.getDefault().postSticky(bus);
        if (!EventBus.getDefault().isRegistered(activity))
            EventBus.getDefault().registerSticky(activity);
    }

}
