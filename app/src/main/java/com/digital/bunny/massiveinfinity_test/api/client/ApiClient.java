package com.digital.bunny.massiveinfinity_test.api.client;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;

/**
 * Created by jayson on 6/25/15.
 */
public interface ApiClient {

        @GET(ApiUrl.Deliveries)
        void getData(Callback<Response> callback);

}
