package com.digital.bunny.massiveinfinity_test.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.digital.bunny.massiveinfinity_test.R;
import com.digital.bunny.massiveinfinity_test.models.DataDetail;
import com.digital.bunny.massiveinfinity_test.utils.BusHelper;
import com.digital.bunny.massiveinfinity_test.utils.Helper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import de.greenrobot.event.EventBus;

/**
 * Created by jayson on 2/23/16.
 */
public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private DataDetail dataDetail;
    private TextView description;
    private ImageView img;

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().registerSticky(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        description = (TextView) findViewById(R.id.txt);
        img = (ImageView) findViewById(R.id.img);
    }

    public void onEvent(BusHelper busHelper){
        switch (busHelper.busEvent){
            case VIEW_MAP:
                dataDetail = (DataDetail) busHelper.busObject;
                description.setText(dataDetail.getDescription());
                Helper.setImage(dataDetail.getImageUrl(), img);
                animatePanel();
                break;
        }
    }

    private void animatePanel(){
        YoYo.with(Techniques.BounceInDown)
                .duration(2000)
                .interpolate(new AccelerateDecelerateInterpolator())
                .playOn(findViewById(R.id.detail_panel));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if(dataDetail!=null){
            mapMove(dataDetail.getLocation().getAddress(),
                    dataDetail.getLocation().getLat(),
                    dataDetail.getLocation().getLng());
        }else
            mapMove("Marker in Sydney", -34, 151);
    }

    private void mapMove(String name, double lat, double longi){
        LatLng latLng = new LatLng(lat, longi);
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(name)
                .snippet(Double.toString(lat)+ ", " +Double.toString(longi)));
        marker.showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
