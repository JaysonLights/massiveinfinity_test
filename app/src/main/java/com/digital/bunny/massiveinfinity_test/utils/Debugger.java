package com.digital.bunny.massiveinfinity_test.utils;

import android.util.Log;

/**
 * Created by jayson on 11/21/14.
 */
public class Debugger {

    private static final boolean DEBUG = true;

    public static void i(String logTag, String logMsg) {
        try {
            if(DEBUG)
            Log.i(logTag, logMsg);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void e(String logTag, String logMsg) {
        try {
            if(DEBUG)
            Log.e(logTag, logMsg);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void d(String logTag, String logMsg) {
        try {
            if(DEBUG)
            Log.d(logTag, logMsg);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
