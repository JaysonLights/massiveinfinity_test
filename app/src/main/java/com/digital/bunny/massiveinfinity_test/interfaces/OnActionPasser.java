package com.digital.bunny.massiveinfinity_test.interfaces;

import com.digital.bunny.massiveinfinity_test.utils.DataHolder;

/**
 * Created by jayson on 11/12/14.
 */
public interface OnActionPasser {
    void onPassedDatas(DataHolder dataHolder);
}
