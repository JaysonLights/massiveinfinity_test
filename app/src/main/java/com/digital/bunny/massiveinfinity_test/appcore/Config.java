package com.digital.bunny.massiveinfinity_test.appcore;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by jayson on 11/26/14.
 */
public class Config {

    private static final String TAG = Config.class.getName();

    private static String environment;

    public static void configure(Context context) {
        try {
            ApplicationInfo ai = context.getPackageManager()
                    .getApplicationInfo(context.getPackageName(),
                            PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            String environment = bundle.getString("com.digital.bunny.massiveinfinity_test.environment");
            setEnvironment(environment);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG,
                    "Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e) {
            Log.e(TAG,
                    "Failed to load meta-data, NullPointer: " + e.getMessage());
        }
    }

    public static String getEnvironment(Context context) {
        if (environment == null)
            configure(context);
        return environment;
    }

    public static void setEnvironment(String paramEnvironment) {
        environment = paramEnvironment;
    }

}
