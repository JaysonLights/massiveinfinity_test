
package com.digital.bunny.massiveinfinity_test.api.client;

import retrofit.RetrofitError;
import retrofit.client.Response;

public abstract class ResponseListener implements CancellableCallback<Response> {

    private boolean mCancel;
    private Object mTag;
    
    @SuppressWarnings("unused")
    protected ResponseListener() {
        
    }
    
    public ResponseListener(Object tag) {
        mTag = tag;
        CallbackManager.getInstance().addToList(this);
    }

    @Override
    public void success(Response arg0, Response arg1) {
        if (isCancelled()) {
        } else {
            requestSuccess(arg0, arg1);
        }
        onRequestFinished();
    }

    @Override
    public void failure(RetrofitError arg0) {
        if (isCancelled()) {
        } else {
            requestFailed(arg0);
        }
        onRequestFinished();
    }
    
    public void onRequestFinished(){
        CallbackManager.getInstance().removeFromCallbackList(mTag);
    }

    public abstract void requestSuccess(Response arg0, Response arg1);
    public abstract void requestFailed(RetrofitError arg0);
    

    @Override
    public Object getTag() {
        return mTag;
    }

    @Override
    public void setTag(Object tag) {
        mTag = tag;
    }

    @Override
    public boolean isCancelled() {
        return mCancel;
    }

    @Override
    public void cancel() {
        mCancel = true;
    }

}
