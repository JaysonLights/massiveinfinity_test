//package com.digital.bunny.massiveinfinity_test.fragments;
//
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.widget.SwipeRefreshLayout;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ListView;
//
//import com.digital.bunny.massiveinfinity_test.R;
//import com.digital.bunny.massiveinfinity_test.adapter.ItemAdapter;
//import com.digital.bunny.massiveinfinity_test.api.deserializers.Key;
//import com.digital.bunny.massiveinfinity_test.models.DataDetail;
//import com.digital.bunny.massiveinfinity_test.utils.BusHelper;
//import com.digital.bunny.massiveinfinity_test.utils.Helper;
//
//import java.util.ArrayList;
//
//import de.greenrobot.event.EventBus;
//import io.realm.Realm;
//import io.realm.RealmResults;
//import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
//import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
//
///**
// * Created by jayson on 2/22/16.
// */
//public class FaveFragment extends Fragment {
//
//    private RecyclerView recyclerView;
//    private SwipeRefreshLayout refreshLayout;
//    private LinearLayoutManager layoutManager;
//    private ArrayList<DataDetail> listData;
//    private ItemAdapter itemAdapter;
//    private Realm realm;
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        if(!EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().registerSticky(this);
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        realm = Realm.getInstance(getActivity());
//    }
//
//    public void onEvent(BusHelper busHelper){}
//
//    public void onEvent(BusHelper.BusEvent busEvent){
//        switch (busEvent){
//            case ACTION_FAVE:
//            case ACTION_UNFAVE:
//                setData();
//                break;
//        }
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_layout, container, false);
//        set(view);
//
//        return view;
//    }
//
//    private void set(View view){
//        initView(view);
//        setData();
//        if(listData.size()==0) Helper.toasterCentered(getActivity(), "No favorite found");
//    }
//
//    private void initView(View view){
//        refreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_refresh_container);
//        recyclerView = (RecyclerView)view.findViewById(R.id.listview);
//        recyclerView.setOverScrollMode(ListView.OVER_SCROLL_NEVER);
//        recyclerView.setHasFixedSize(true);
//        layoutManager = new LinearLayoutManager(getContext());
//        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        recyclerView.setLayoutManager(layoutManager);
//        refreshLayout.setOnRefreshListener(refresh);
//    }
//
//    public void setData() {
//        initDataList();
//        itemAdapter = new ItemAdapter(getActivity(), listData, recyclerView);
//        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(itemAdapter);
//        recyclerView.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));
//        itemAdapter.notifyDataSetChanged();
//    }
//
//    private void initDataList(){
//        listData = new ArrayList<>();
//        RealmResults<DataDetail> data = realm.where(DataDetail.class).equalTo(Key.isFave, true).findAll();
//
//        if(data==null)
//            Helper.toasterCentered(getActivity(), "No favourite found.");
//        else
//            listData.addAll(data);
//    }
//
//    private SwipeRefreshLayout.OnRefreshListener refresh = new SwipeRefreshLayout.OnRefreshListener() {
//        @Override
//        public void onRefresh() {
//            setData();
//            refreshLayout.setRefreshing(false);
//            if(listData.size()==0) Helper.toasterCentered(getActivity(), "No favorite found");
//        }
//    };
//
//}
