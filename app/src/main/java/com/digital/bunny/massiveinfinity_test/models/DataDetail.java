package com.digital.bunny.massiveinfinity_test.models;


import com.digital.bunny.massiveinfinity_test.api.deserializers.Key;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by jayson on 2/22/16.
 */
public class DataDetail extends RealmObject {

    @SerializedName(Key.Description)
    private String description;

    @SerializedName(Key.ImageUrl)
    private String imageUrl;

    @SerializedName(Key.Location)
    private LocDetail location;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public LocDetail getLocation() {
        return location;
    }

    public void setLocation(LocDetail location) {
        this.location = location;
    }
}
