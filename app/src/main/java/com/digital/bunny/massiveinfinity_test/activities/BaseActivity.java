package com.digital.bunny.massiveinfinity_test.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.digital.bunny.massiveinfinity_test.api.core.ClientManager;
import com.digital.bunny.massiveinfinity_test.appcore.App;
import com.digital.bunny.massiveinfinity_test.utils.Helper;

import io.realm.Realm;

public abstract class BaseActivity extends ActionBarActivity {

    private App coreApp;
    private ClientManager clientManager;
    private Realm realm;
    private ProgressDialog progressDialog;

    protected abstract void initLayout();
    protected abstract void initLayoutActionListeners();
    protected abstract void initData();

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        init();
    }

    private void init(){
        coreApp = (App) getApplication();
        clientManager = coreApp.getClientManager();
        realm = coreApp.getRealm();
        setProgressDialog(Helper.initProgressDialog(this));
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        initLayout();
        initLayoutActionListeners();
        initData();
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    public ClientManager getClientManager() {
        return clientManager;
    }

    public Realm getRealm(){
        return realm;
    }

    public void toaster(String message, int displayTime){
        Toast.makeText(this, message, displayTime).show();
    }

    public void toaster(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void toasterCentered(String message){
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if( v != null) v.setGravity(Gravity.CENTER);
        toast.show();
    }

    public void toasterCentered(String message, int displayTime){
        Toast toast = Toast.makeText(this, message, displayTime);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if( v != null) v.setGravity(Gravity.CENTER);
        toast.show();
    }
}
