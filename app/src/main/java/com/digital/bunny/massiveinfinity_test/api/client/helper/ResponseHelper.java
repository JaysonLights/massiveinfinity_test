package com.digital.bunny.massiveinfinity_test.api.client.helper;

import android.content.Context;

import com.digital.bunny.massiveinfinity_test.api.client.ResponseListener;
import com.digital.bunny.massiveinfinity_test.api.core.api_helper.OnApiResponse;
import com.digital.bunny.massiveinfinity_test.api.core.api_helper.ResponsePair;
import com.digital.bunny.massiveinfinity_test.models.DataDetail;
import com.digital.bunny.massiveinfinity_test.utils.Debugger;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by jayson on 3/30/15.
 */
public class ResponseHelper {

    public static final String NOT_A_JSON_ERROR = "Invalid response. Not a JSON.";

    private static void onRequestFailed(OnApiResponse onApiResponse, Object tag, RetrofitError error){
        Debugger.e(tag + "-" + "onRequestError", "Server is unreachable...");
        onApiResponse.onRequestError(error, tag);
    }

    public static ResponseListener getList(final Context context, final Object tag, final Gson gson,
                                                 final OnApiResponse<ResponsePair<List<DataDetail>>> onApiResponse){
        ResponseListener questionList = new ResponseListener(tag) {
            @Override
            public void requestSuccess(Response arg0, Response arg1) {
                InputStreamReader reader = DeserializerUtils.getReaderFromResponse(arg0);
                try{
                    final Type type = new TypeToken<ResponsePair<List<DataDetail>>>() {}.getType();
                    ResponsePair<List<DataDetail>> apiResponse = gson.fromJson(reader, type);
                    apiResponse.getExtras().setTag(tag);

                    String details = DataUtils.getRawDetailResponse(context, apiResponse.getExtras().getRawJsonArrayResult());
                    Debugger.i("ApiCalls", tag + " Api call is successful: " + apiResponse.getExtras().isStatusSuccess() + "\n" + details);

                    onApiResponse.onRequestSuccessful(apiResponse);
                }
                catch(JsonSyntaxException e){
                    Debugger.e(tag+"-"+"onRequestFailed", NOT_A_JSON_ERROR);
                    onApiResponse.onRequestFailed(NOT_A_JSON_ERROR, tag);
                }
                finally {
                    DeserializerUtils.closeReader(reader);
                }

            }

            @Override
            public void requestFailed(RetrofitError error) {
                Debugger.e(tag+"-"+"onRequestFailed", "requestFailed");
                onRequestFailed(onApiResponse, tag, error);
            }
        };

        return questionList;
    }

}
