package com.digital.bunny.massiveinfinity_test.api.client.helper;

import android.content.Context;

import com.digital.bunny.massiveinfinity_test.api.core.api_helper.ApiResponse;
import com.digital.bunny.massiveinfinity_test.api.deserializers.Key;
import com.digital.bunny.massiveinfinity_test.models.DataDetail;
import com.digital.bunny.massiveinfinity_test.models.LocDetail;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import io.realm.Realm;
import retrofit.client.Response;

public class DeserializerUtils {

    private Context context;

    public DeserializerUtils(Context context){
        this.context = context;
    }

    public static ApiResponse extras(JsonObject extras){
        ApiResponse response = new ApiResponse();
        response.setRawJsonResult(extras);

        response.setStatusSuccess(!extras.has(Key.errorReasonPhrase));

        response.setErrorCode(extras.has(Key.errorCode) ?
                extras.get(Key.errorCode).getAsInt() : 0);

        response.setErrorDescription(extras.has(Key.errorDescription) ?
                extras.get(Key.errorDescription).getAsString() : "");

        response.setErrorReasonPhrase(extras.has(Key.errorReasonPhrase) ?
                extras.get(Key.errorReasonPhrase).getAsString() : "");

        return response;
    }

    public static ApiResponse extras(JsonArray jsonArray){
        ApiResponse response = new ApiResponse();
        String errorResponse = "No Data return";
        boolean hasData = jsonArray.size()!=0;

        response.setRawJsonArrayResult(jsonArray);
        response.setHasData(hasData);
        response.setStatusSuccess(true);
        response.setErrorDescription(hasData ? "" : errorResponse);
        response.setErrorReasonPhrase(hasData ? "" : errorResponse);

        return response;
    }

    public static InputStreamReader getReaderFromResponse(Response response){
        try {
            InputStreamReader reader = new InputStreamReader(response.getBody().in(), "UTF-8");
            return reader;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void closeReader(InputStreamReader reader){
        try{
            reader.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

    public static DataDetail updateDataItem(Context context, Realm realm, JsonObject jsonObject){
        DataDetail item;
        realm.beginTransaction();

        item = realm.where(DataDetail.class)
                .equalTo(Key.Description, DataUtils.stringJsonNullChecker(jsonObject, Key.Description)).findFirst();

        if(item==null)
            item = realm.createObject(DataDetail.class);

        item.setDescription(DataUtils.stringJsonNullChecker(jsonObject, Key.Description));
        item.setImageUrl(DataUtils.stringJsonNullChecker(jsonObject, Key.ImageUrl));

        JsonObject loc = jsonObject.getAsJsonObject(Key.Location);
        LocDetail locDetail = realm.createObject(LocDetail.class);
        locDetail.setAddress(DataUtils.stringJsonNullChecker(loc, Key.Address));
        locDetail.setLat(DataUtils.doubleJsonNullChecker(loc, Key.Lat));
        locDetail.setLng(DataUtils.doubleJsonNullChecker(loc, Key.Lng));
        item.setLocation(locDetail);

        realm.commitTransaction();

        return item;
    }

}
