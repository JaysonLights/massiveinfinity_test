package com.digital.bunny.massiveinfinity_test.models;


import com.digital.bunny.massiveinfinity_test.api.deserializers.Key;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by jayson on 2/22/16.
 */
public class LocDetail extends RealmObject {

    @SerializedName(Key.Lat)
    private double lat;

    @SerializedName(Key.Lng)
    private double lng;

    @SerializedName(Key.Address)
    private String address;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
