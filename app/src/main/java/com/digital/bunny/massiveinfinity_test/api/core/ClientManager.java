package com.digital.bunny.massiveinfinity_test.api.core;

import android.content.Context;

import com.digital.bunny.massiveinfinity_test.api.client.ApiClient;
import com.digital.bunny.massiveinfinity_test.api.client.ApiUrl;
import com.digital.bunny.massiveinfinity_test.api.client.CallbackManager;
import com.digital.bunny.massiveinfinity_test.api.client.helper.ResponseHelper;
import com.digital.bunny.massiveinfinity_test.api.core.api_calls.ApiCall;
import com.digital.bunny.massiveinfinity_test.api.core.api_helper.OnApiResponse;
import com.digital.bunny.massiveinfinity_test.api.core.api_helper.ResponsePair;
import com.digital.bunny.massiveinfinity_test.api.deserializers.DataListDeserializer;
import com.digital.bunny.massiveinfinity_test.models.DataDetail;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import io.realm.Realm;
import retrofit.RestAdapter;
import retrofit.android.AndroidLog;

/**
 * Created by jayson on 6/25/15.
 */
public class ClientManager implements ApiCall {

    private Context mContext;
    private RestAdapter restAdapter;
    private Gson gson;
    private Realm realm;
    private ApiClient apiClient;

    public ClientManager(Context context) {
        mContext = context;
        realm = Realm.getInstance(mContext);
        init();
    }

    private void init(){
        restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(new AndroidLog("LogLevel_FULL"))
                .setEndpoint(ApiUrl.BASE_API_URL)
                .build();

        Type dataListType = new TypeToken<ResponsePair<List<DataDetail>>>(){}.getType();

        gson = new GsonBuilder()
                .registerTypeAdapter(dataListType, new DataListDeserializer(mContext, realm))
                .create();

        apiClient = restAdapter.create(ApiClient.class);
    }

    // Cancel specific response
    public void cancel(Object tag){
        CallbackManager.getInstance().cancel(tag);
    }

    // Cancel all response from retrofit
    public void cancelAll(){
        CallbackManager.getInstance().cancelAll();
    }

    @Override
    public void getData(Object tag, OnApiResponse<ResponsePair<List<DataDetail>>> onApiResponse) {
        apiClient.getData(ResponseHelper.getList(mContext, tag, gson, onApiResponse));
    }
}
