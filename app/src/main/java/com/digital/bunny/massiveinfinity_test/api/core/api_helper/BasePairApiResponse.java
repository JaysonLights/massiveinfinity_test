package com.digital.bunny.massiveinfinity_test.api.core.api_helper;

public class BasePairApiResponse<E, R> {

    private final R result;
    private final E extras;


    public BasePairApiResponse(E extras, R result) {
        this.extras = extras;
        this.result = result;
    }

    public R getResult() {
        return result;
    }

    public E getExtras() {
        return extras;
    }


}
