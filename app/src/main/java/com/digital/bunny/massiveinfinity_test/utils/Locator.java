package com.digital.bunny.massiveinfinity_test.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.digital.bunny.massiveinfinity_test.interfaces.OnActionPasser;

/**
 * Created by jayson on 2/24/16.
 */
public class Locator {

    private Context context;
    private OnActionPasser actionPasser;
    private LocationManager locationManager;
    private LocationListener locationListenerGps;
    private LocationListener locationListenerNetwork;
    boolean gps_enabled = false;
    boolean network_enabled = false;

    public Locator(Context context, OnActionPasser actionPasser){
        this.context = context;
        this.actionPasser = actionPasser;
        checkService();
    }

    private void checkService(){
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            locate();
        }else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setMessage("Please enable your data connection and restart the application.");
            alertDialogBuilder.setNeutralButton("Okay",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            actionPasser.onPassedDatas(new DataHolder(Tag.TagEnum.TAG_ENABLE_INTERNET));
                            arg0.dismiss();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    private void locate(){
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        // exceptions will be thrown if provider is not permitted.
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!gps_enabled && !network_enabled) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setMessage("It seems like your Location Services is disabled.");
            alertDialogBuilder.setNeutralButton("Enable Location Services.",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            actionPasser.onPassedDatas(new DataHolder(Tag.TagEnum.TAG_ENABLE_LOCATION_SERVICE));
                            arg0.dismiss();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else {
            actionPasser.onPassedDatas(new DataHolder(Tag.TagEnum.TAG_PROGRESS_LOADER));
        }

        locationListenerGps = locationListener(0);
        locationListenerNetwork = locationListener(1);

        // if gps is enabled, get location updates
        if (gps_enabled) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
        }

        // if network is enabled, get location updates
        if (network_enabled) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
        }

    }

    private LocationListener locationListener(final int locatorType){
        //locatorType = 0 for GPS -> cancels locationListenerNetwork if locating done
        //locatorType = 1 for Network -> cancels locationListenerGps if locating done
        return new LocationListener() {

            public void onLocationChanged(Location currentLocation) {
                double lat = currentLocation.getLatitude();
                double longi = currentLocation.getLongitude();

                // store coordinates
                PrefHelper.putDoubleData(context, Tag.user_long, currentLocation.getLongitude());
                PrefHelper.putDoubleData(context, Tag.user_lat, currentLocation.getLatitude());

//                Helper.toasterCentered(context,
//                        String.valueOf(PrefHelper.getDoubleData(context, Tag.user_long)) + " " +
//                                String.valueOf(PrefHelper.getDoubleData(context, Tag.user_lat)));

                // if you want to stop listening to gps location updates, un-comment the code below
                locationManager.removeUpdates(this);
                locationManager.removeUpdates(locatorType==0?locationListenerNetwork:locationListenerGps);
                actionPasser.onPassedDatas(new DataHolder(Tag.TagEnum.TAG_END_LOADER));
            }

            public void onProviderDisabled(String provider) {}
            public void onProviderEnabled(String provider) {}
            public void onStatusChanged(String provider, int status, Bundle extras) {}
        };
    }

}
