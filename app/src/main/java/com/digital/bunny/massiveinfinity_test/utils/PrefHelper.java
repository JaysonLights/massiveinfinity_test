package com.digital.bunny.massiveinfinity_test.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by jayson on 2/23/16.
 */
public class PrefHelper {

    private static final String PREF_NAME = "BUUUK_TEST";

    public static void putStringData(Context context, String key, String value){
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = prefs.edit();
        ed.putString(key, value);
        ed.commit();
    }

    public static void putDoubleData(Context context, String key, double value){
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = prefs.edit();
        ed.putLong(key, Double.doubleToRawLongBits(value));
        ed.commit();
    }

    public static String getStringData(Context context, String key){
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return prefs.getString(key, "no value");
    }

    public static double getDoubleData(Context context, String key){
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return Double.longBitsToDouble(prefs.getLong(key, Double.doubleToLongBits(0)));
    }

}
