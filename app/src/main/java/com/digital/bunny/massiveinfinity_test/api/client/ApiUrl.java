package com.digital.bunny.massiveinfinity_test.api.client;

public class ApiUrl {

    private String apiKey;
    public static String BASE_API_URL;

    private final String LOC = "local";
    private final String DEV = "develop";
    private final String PROD = "production";

    private static final String LOC_API_URL = "http://mi-mobile-dev.ap-southeast-1.elasticbeanstalk.com";
    private static final String DEV_API_URL = "http://mi-mobile-dev.ap-southeast-1.elasticbeanstalk.com";
    private static final String PROD_API_URL = "http://mi-mobile-dev.ap-southeast-1.elasticbeanstalk.com";


    // Initialize the API's using the correct environment.
    public void init(String environment) {
        if (environment.equals(LOC)) {
            BASE_API_URL = LOC_API_URL;
        } else if (environment.equals(DEV)) {
            BASE_API_URL = DEV_API_URL;
        } else{
            BASE_API_URL = PROD_API_URL;
        }
    }

    public ApiUrl(String environment, String apiKey) {
        init(environment);
        this.apiKey = apiKey;
    }

    //Api endpoints
    public static final String Deliveries = "/deliveries";
}
