
package com.digital.bunny.massiveinfinity_test.utils;

import android.content.Context;

import com.digital.bunny.massiveinfinity_test.R;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

public class ImageCacheConfig {
	
	/**
	 * Initialize the image cache.
	 * 
	 * @param context
	 */
	public static void init(Context context) {
		if (!ImageLoader.getInstance().isInited()) {
			ImageLoaderConfiguration imgLoaderConfig = new ImageLoaderConfiguration.Builder(context)
			.threadPoolSize(5)// default is 5
			.threadPriority(Thread.NORM_PRIORITY - 1)
			.tasksProcessingOrder(QueueProcessingType.FIFO)
			.memoryCacheSizePercentage(15)
			.denyCacheImageMultipleSizesInMemory()
			.discCacheFileNameGenerator(new HashCodeFileNameGenerator())
			.discCacheSize(20 * 1024 * 1024)
			.imageDownloader(new BaseImageDownloader(context)).writeDebugLogs().
			build();
			ImageLoader.getInstance().init(imgLoaderConfig);
		}
		
	}
	
	public static DisplayImageOptions getUserGalleryDisplayOptions() {
		DisplayImageOptions optionsForRowImage = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.default_loader_img).cacheInMemory(true)
				.displayer(new CustomFadeInDisplayer(1200, true, false, false)).showImageForEmptyUri(R.drawable.default_loader_img).cacheOnDisc(true).build();
		return optionsForRowImage;
	}


}
