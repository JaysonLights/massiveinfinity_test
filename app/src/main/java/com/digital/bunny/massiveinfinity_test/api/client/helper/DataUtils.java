package com.digital.bunny.massiveinfinity_test.api.client.helper;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Created by jayson on 11/21/14.
 */
public class DataUtils {

    public static String getRawDetailResponse(Context context, JsonObject jObject){
        String details = jObject.toString();
        return details;
    }

    public static String getRawDetailResponse(Context context, JsonArray jsonArray){
        String details = jsonArray.toString();
        return details;
    }

    public static boolean booleanJsonNullChecker(JsonObject jsonObject, String key){
        boolean data = false;
        if(jsonObject.has(key))
            if(!jsonObject.get(key).isJsonNull()){
                data = jsonObject.get(key).getAsBoolean();
            }
        return data;
    }

    public static String stringJsonNullChecker(JsonObject jsonObject, String key){
        String data ="";
        if(jsonObject.has(key))
            if(!jsonObject.get(key).isJsonNull()){
                data = jsonObject.get(key).getAsString();
            }
        return data;
    }

    public static long longJsonNullChecker(JsonObject jsonObject, String key){
        long data = 0;
        if(jsonObject.has(key))
            if(!jsonObject.get(key).isJsonNull()){
                data = jsonObject.get(key).getAsLong();
            }
        return data;
    }

    public static double doubleJsonNullChecker(JsonObject jsonObject, String key){
        double data = 0;
        if(jsonObject.has(key))
            if(!jsonObject.get(key).isJsonNull()){
                data = jsonObject.get(key).getAsDouble();
            }
        return data;
    }

    public static float floatJsonNullChecker(JsonObject jsonObject, String key){
        float data = 0;
        if(jsonObject.has(key))
            if(!jsonObject.get(key).isJsonNull()){
                data = jsonObject.get(key).getAsFloat();
            }
        return data;
    }
}
