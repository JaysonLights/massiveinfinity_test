
package com.digital.bunny.massiveinfinity_test.api.client;

import retrofit.Callback;

public interface CancellableCallback<T> extends Callback<T> {
    
    public Object getTag();
    public void setTag(Object tag);
    public void cancel();
    public boolean isCancelled();

}
