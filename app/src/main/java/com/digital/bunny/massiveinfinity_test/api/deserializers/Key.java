package com.digital.bunny.massiveinfinity_test.api.deserializers;

/**
 * Created by jayson on 6/25/15.
 */
public class Key {

    public static final String errorCode = "code";
    public static final String errorDescription = "description";
    public static final String errorReasonPhrase = "reasonPhrase";
    public static final String timeStamp = "timeStamp";
    public static final String ctrlType = "ctrlType";

    //pagination sort ctrl
    public static final String DESC = " DESC";
    public static final String ASC = " ASC";

    //detail
    public static final String Description = "description";
    public static final String ImageUrl = "imageUrl";
    public static final String Location = "location";
    public static final String Lat = "lat";
    public static final String Lng = "lng";
    public static final String Address = "address";

}
