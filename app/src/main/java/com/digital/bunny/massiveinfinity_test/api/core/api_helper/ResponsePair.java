package com.digital.bunny.massiveinfinity_test.api.core.api_helper;

public class ResponsePair<T> extends BasePairApiResponse<ApiResponse, T>{

    public ResponsePair(ApiResponse first, T second) {
        super(first, second);
    }

}
