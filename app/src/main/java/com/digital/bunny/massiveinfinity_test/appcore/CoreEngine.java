package com.digital.bunny.massiveinfinity_test.appcore;

import android.content.Context;

import com.digital.bunny.massiveinfinity_test.api.core.ClientManager;

/**
 * Created by jayson on 11/26/14.
 */
public class CoreEngine {

    private boolean isStarted;
    private ClientManager apiBoss;

    public void start(Context context) {
        String environment = Config.getEnvironment(context);
        Configuration.init(environment, null);
        apiBoss = new ClientManager(context);
    }

    public ClientManager getApiBoss(){
        return apiBoss;
    }

    public CoreEngine(Context context) {
        if (!isStarted) {
            start(context);
            isStarted = true;
        }

    }

}
