package com.digital.bunny.massiveinfinity_test.utils;

/**
 * Created by jayson on 6/10/15.
 */
public class DataHolder {

    public static final String preExecute = "preExecute";
    public static final String doInBackground = "doInBackground";
    public static final String progressUpdate = "progressUpdate";
    public static final String postExecute = "postExecute";

    public Tag.TagEnum tag;
    public String state;
    public Object passData;

    public DataHolder(Tag.TagEnum tag) {
        this.tag = tag;
    }

    public DataHolder(Tag.TagEnum tag, Object passData) {
        this.tag = tag;
        this.passData = passData;
    }

    public DataHolder(Tag.TagEnum tag, String state, Object passData) {
        this.tag = tag;
        this.state = state;
        this.passData = passData;
    }

}
