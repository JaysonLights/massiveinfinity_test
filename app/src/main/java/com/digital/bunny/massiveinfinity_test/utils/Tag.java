package com.digital.bunny.massiveinfinity_test.utils;

/**
 * Created by jayson on 5/22/15.
 */
public class Tag {
    public  static final String user_lat = "user_lat";
    public  static final String user_long = "user_long";

    public enum TagEnum{
        //For Apis

        //Custom PopUp Dialog

        //Tags
        TAG_GET_LOCATION, TAG_ENABLE_INTERNET, TAG_ENABLE_LOCATION_SERVICE,
        TAG_PROGRESS_LOADER, TAG_END_LOADER
    }
}
