package com.digital.bunny.massiveinfinity_test.appcore;


import com.digital.bunny.massiveinfinity_test.api.client.ApiUrl;

/**
 * Created by jayson on 11/26/14.
 */
public class Configuration {

    private static Configuration configuration;

    public static Configuration init(String environment, String apiKey) {
        if (configuration == null) {
            configuration = new Configuration(environment, apiKey);
        }
        return configuration;
    }

    public Configuration(String environment, String apiKey) {
        new ApiUrl(environment, apiKey);
    }

}
