package com.digital.bunny.massiveinfinity_test.activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.digital.bunny.massiveinfinity_test.R;
import com.digital.bunny.massiveinfinity_test.adapter.ItemAdapter;
import com.digital.bunny.massiveinfinity_test.api.core.api_helper.OnApiResponse;
import com.digital.bunny.massiveinfinity_test.api.core.api_helper.ResponsePair;
import com.digital.bunny.massiveinfinity_test.models.DataDetail;
import com.digital.bunny.massiveinfinity_test.utils.BusHelper;
import com.digital.bunny.massiveinfinity_test.utils.Helper;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import io.realm.RealmResults;
import retrofit.RetrofitError;

public class MainActivity extends BaseActivity {

    private Toolbar toolbar;
    private TextView wait;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayoutManager layoutManager;
    private ArrayList<DataDetail> listData;
    private ItemAdapter itemAdapter;

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().registerSticky(this);
    }

    public void onEvent(BusHelper busHelper){}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    @Override
    protected void initLayout() {
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorAccent));
        toolbar.setTitle("Things to Deliver");
        setSupportActionBar(toolbar);

        wait = (TextView) findViewById(R.id.wait);
        refreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_container);
        recyclerView = (RecyclerView)findViewById(R.id.listview);
        recyclerView.setOverScrollMode(ListView.OVER_SCROLL_NEVER);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        refreshLayout.setOnRefreshListener(refresh);

        toggleView(false);
    }

    private void toggleView(boolean viewlist){
        wait.setVisibility(viewlist? View.GONE:View.VISIBLE);
        refreshLayout.setVisibility(viewlist? View.VISIBLE:View.GONE);
    }

    @Override
    protected void initLayoutActionListeners() {

    }

    private SwipeRefreshLayout.OnRefreshListener refresh = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            if(Helper.isConnected(MainActivity.this)) {
                getData();
            }else{
                refreshLayout.setRefreshing(false);
                Helper.toasterCentered(MainActivity.this, "No internet connection");
            }
        }
    };

    @Override
    protected void initData() {
        listData = new ArrayList<>();

        RealmResults<DataDetail> data = getRealm().where(DataDetail.class).findAll();
        if(data!=null) listData.addAll(data);

        if(Helper.isConnected(this)) {
            getData();
        }else{
            Helper.toasterCentered(this, "Please connect to the internet to update data.");
        }

        setData();

    }

    public void setData() {
        toggleView(listData.size()!=0);
        itemAdapter = new ItemAdapter(this, listData, recyclerView);
        recyclerView.setAdapter(itemAdapter);
        itemAdapter.notifyDataSetChanged();
    }

    private void getData(){
        //Helper.toasterCentered(this, "Please wait..");
        getClientManager().getData("TEST_DATA", onDataFetch);
    }

    private OnApiResponse<ResponsePair<List<DataDetail>>> onDataFetch = new OnApiResponse<ResponsePair<List<DataDetail>>>() {
        @Override
        public void onRequestSuccessful(ResponsePair<List<DataDetail>> result) {
            listData.clear();
            listData.addAll(result.getResult());
            toggleView(listData.size()!=0);
            itemAdapter.reloadItems(listData);
            refreshLayout.setRefreshing(false);
            itemAdapter.setLoaded();
        }

        @Override
        public void onRequestFailed(String errorMessage, Object tag) {
            toggleView(true);
            refreshLayout.setRefreshing(false);
            toasterCentered(getString(R.string.error_message));
        }

        @Override
        public void onRequestError(RetrofitError error, Object tag) {
            toggleView(true);
            refreshLayout.setRefreshing(false);
            toasterCentered(getString(R.string.error_message));
        }
    };

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

}
